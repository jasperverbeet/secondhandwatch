.PHONY: up down build create-db clean

up: 
	@make create-db
	@echo "Starting development, for production: '\033[0;33mgit checkout release\033[0m'"
	@docker-compose -f docker-compose-dev.yml up -d --force-recreate --remove-orphans; \

down:
	@docker-compose -f docker-compose-dev.yml down -v --remove-orphans

build:
	@make down
	docker image rm registry.gitlab.com/jasperverbeet/secondhandwatch
	docker build -t registry.gitlab.com/jasperverbeet/secondhandwatch ./

create-db:
	@if ! docker volume ls | grep "database" &> /dev/null; then \
		echo "Creating volume \"database\" ..."; \
		docker volume create --name=database &> /dev/null; \
	fi \

clean:
	find . -name "*.pyc" -exec rm -f {} \;
	find . -name "__pycache__" -exec rm -rf {} \;