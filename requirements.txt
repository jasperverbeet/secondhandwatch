amqp==2.3.2
argh==0.26.2
astroid==2.0.4
billiard==3.5.0.4
celery==4.2.1
certifi==2018.8.24
chardet==3.0.4
Django==2.1.2
django-celery-results==1.0.1
django-cors-middleware==1.3.1
django-filter==2.0.0
django-oauth-toolkit==1.2.0
djangorestframework==3.8.2
idna==2.7
isort==4.3.4
kombu==4.2.1
lazy-object-proxy==1.3.1
Markdown==3.0.1
mccabe==0.6.1
oauthlib==2.1.0
pathtools==0.1.2
pylint==2.1.1
pytz==2018.5
PyYAML==3.13
requests==2.19.1
six==1.11.0
urllib3==1.23
vine==1.1.4
watchdog==0.9.0
wrapt==1.10.11
django-celery-beat==1.1.1
beautifulsoup4==4.6.3
urllib3==1.23
mysqlclient==1.3.13