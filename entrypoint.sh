#!/bin/bash

echo -e "\033[0;33mRunning migrations...\033[0m"
python manage.py migrate
python3 manage.py runserver 0.0.0.0:8000