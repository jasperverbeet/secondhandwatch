#!/usr/bin/env python
"""
This module is used for subscription related views.
"""

import json

from django_celery_beat.models import PeriodicTask, IntervalSchedule

from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status, viewsets

from django.shortcuts import get_object_or_404

from secondhandwatch.models.subscription import SubscriptionModel
from secondhandwatch.serializers.subscription import SubscriptionSerializer, QuerySerializer

class SubscriptionViewSet(viewsets.ViewSet):
    """
    This viewset handles everything regarding subscriptions. Users can subscribe
    to new topics (queries) and list them using this viewset.
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, primary_key=None):
        """Retrieve a subscription based on PK and the current user."""

        if primary_key is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        subscription = get_object_or_404(
            SubscriptionModel,
            pk=primary_key,
            user=request.user)
        serializer = SubscriptionSerializer(subscription)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def subscribe(self, request):
        """Subscribe to a new query."""

        # name = request.POST.get('name', None)
        # interval = request.POST.get('interval', None)
        # search = request.POST.get('search_query', None)
        # # Make sure these values are not none
        # if not name or not interval or not search:
        #     return Response(status=status.HTTP_400_BAD_REQUEST)

        # Validate request data
        serializer = QuerySerializer(data=request.data)

        if not serializer.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST)

        # Create new instance or return one if exists
        instance, _ = serializer.get_or_create()
        
        # Also add the subscription
        SubscriptionModel(user=request.user, query=instance).save()

        # Create a schedule
        schedule, __ = IntervalSchedule.objects.get_or_create(
            every=instance.interval.seconds,
            period=IntervalSchedule.SECONDS,
        )

        # Add task to the schedule
        PeriodicTask.objects.create(
            interval=schedule,
            name='Request: {}'.format(instance.name),
            task='secondhandwatch.tasks.tasks.get_advertisements',
            args=json.dumps([instance.id]),
        )

        return Response(status=status.HTTP_200_OK)
