from urllib.parse import urlencode

import re
import logging
import urllib3

from bs4 import BeautifulSoup
from secondhandwatch.tasks.models import advert

urllib3.disable_warnings()

class HttpManager(object):

    def __init__(self):
        self.http = urllib3.PoolManager()

    def _get_url_soup(self, url):
        page = self.http.request('GET', url)
        return BeautifulSoup(page.data, 'html.parser')


class AdManager(HttpManager):

    def find(self, ad_id):
        ad_id = re.sub(r'a|m', '', ad_id)
        
        articles = self._search(ad_id)
        search_result_articles = [ad for ad in filter(
            lambda art: ad_id in art.get('data-item-id'), articles)]

        if not search_result_articles:
            return None
        
        if len(search_result_articles) > 1:
            logging.warning('Found multiple results, assuming first result')

        advertisement_url = search_result_articles[0].get('data-url')
        return advert.Advertisement(
            self._get_url_soup(advertisement_url),
            advertisement_url)

    def find_all(self, search, limit=10):
        articles = self._search(search)

        if limit != 0:
            articles = articles[:limit]

        urls = [url for url in map(lambda art: art.get('data-url'), articles)]

        return [advert.Advertisement(
            self._get_url_soup(url), url) for url in urls]


    def _search(self, search):
        encoded_parameters = urlencode(dict(
            query=search,
            categoryId='0',
            postcode='5613KA',
            distance='0',
            numberOfResultsPerPage='100'
        ))
        advertisement_search_url = 'https://www.marktplaats.nl/z.html?{0}'.format(
            encoded_parameters)

        search_soup = self._get_url_soup(advertisement_search_url)       

        return search_soup.find_all('article')
