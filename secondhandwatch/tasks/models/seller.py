import re

class Seller(object):

    def __init__(self, soup):
        self._soup = soup
        self._seller_id = self._get_seller_id()

    def to_dict(self):
        return dict(
            name=self.name,
            years_active=self.active_since,
            location=self.location,
            seller_id=self.seller_id,
            url=self.seller_url,
        )

    def _get_seller_id(self):
        return self._soup.find(attrs={'name': 'sellerId'}).get('value')

    @property
    def seller_url(self):
        return 'https://www.marktplaats.nl/verkopers/{0}.html'.format(self._seller_id)

    def _format_active_since(self, active_since_str):
        if not active_since_str:
            return None

        active_since_str = active_since_str.replace('\u00BD', '.5')

        if 'maand' in active_since_str or 'wek' in active_since_str or 'week' in active_since_str:
            return 0
        
        years = re.findall(r'\d+\.\d+', active_since_str)

        if len(years) == 0:
            return 0

        return float(years[0])

    @property
    def seller_id(self):
        return self._seller_id

    @property
    def name(self):
        return self._soup.find(attrs={'class': 'top-info'}).find(attrs={'class': 'name'}).get('title')

    @property
    def active_since(self):
        active_since = self._soup.find(attrs={'id': 'vip-active-since'}).span.text
        return self._format_active_since(active_since)

    @property
    def location(self):
        return self._soup.find(attrs={'class': 'seller-info-links'}).find(attrs={'class': 'name'}).text