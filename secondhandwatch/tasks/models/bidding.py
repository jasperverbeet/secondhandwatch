from secondhandwatch.utils import parse

class Biddings(object):

    def __init__(self, soup):
        self._soup = soup

    @property
    def iso_date_bids(self):
        bids = self._get_bids()

        return [{
            'name': bid.get('name'),
            'price': bid.get('price'),
            'date': bid.get('date').isoformat()
        } for bid in bids]

    def _get_bids(self):
        bids = self._soup.find_all(attrs={'class': 'bid'})

        return [{
            'name': bid.find(attrs={'class': 'bid-name'}).text,
            'price': parse.parse_money(bid.find(attrs={'class': 'bid-amount'}).text),
            'date': parse.parse_date(bid.find(attrs={'class': 'bid-date'}).text)
        } for bid in bids]

    def to_dict(self):
        return self.iso_date_bids

    @property
    def total_bids(self):
        return len(self._get_bids())

    @property
    def min_bid(self):
        money_el = self._soup.find(attrs={'id': 'vip-bidding-form-min-bid'})

        if not money_el:
            return None

        return parse.parse_money(
            money_el.text
        )

    @property
    def bids(self):
        return self._get_bids()