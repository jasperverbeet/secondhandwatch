from datetime import datetime

import re

from secondhandwatch.utils import parse
from secondhandwatch.tasks.models import bidding, seller


class Advertisement(object):

    def __init__(self, soup, url):
        super()
        self._soup = soup
        self._url = url

    def _get_price_string(self):
        return self._soup.find(attrs={'id': 'vip-ad-price-container'}).span.text

    def _get_shipping_info(self):
        return self._soup.find(attrs={'id': 'shipping-details'})

    def _get_shipping_delivery(self):
        shipping_info = self._get_shipping_info()

        if not shipping_info:
            return None

        return shipping_info.find(attrs={'class': 'shipping-details-value'}).text

    def to_dict(self):
        return dict(
            marktplaats_id=self.advertisement_id,
            is_commercial=self.is_ad,
            title=self.title,
            price=self.price,
            price_str=self.price_str,
            description=self.description,
            specifications=self.specifications,
            images=self.images,
            can_deliver=self.delivered,
            can_collect=self.collected,
            shipping_costs=self.shipping_costs,
            listed=self.listed.isoformat(),
            views=self.view_count,
            bids=[b.to_dict() for b in self.bids],
            seller=self.seller.to_dict(),
            url=self.url,
        )

    @property
    def advertisement_id(self):
        id_el = self._soup.find(attrs={'id': 'search-breadcrumbs-content'})

        if not id_el:
            return 'Unknown'

        id_str = id_el.find('span', attrs={'class': 'crumb'}).text
        return id_str.replace('Advertentie ', '')

    @property
    def url(self):
        return self._url

    @property
    def is_ad(self):
        return 'a' in self.advertisement_id

    @property
    def images(self):    
        ad_images = self._soup.find(attrs={'id': 'vip-carousel'}).get('data-images-l')
        if len(ad_images) == 0:
            return []

        return [{'url': 'https:{0}'.format(url)} for url in ad_images.split('&')]

    @property
    def price(self):
        return parse.parse_money(self._get_price_string())

    @property
    def price_str(self):
        return self._get_price_string()

    @property
    def delivered(self):
        shipping = self._get_shipping_delivery()

        if not shipping:
            return None

        return 'Verzenden' in shipping

    @property
    def collected(self):
        shipping = self._get_shipping_delivery()

        if not shipping:
            return None

        return 'Ophalen' in shipping
    
    @property
    def shipping_costs(self):
        shipping_info = self._get_shipping_info()

        if not shipping_info:
            return None
        
        price_el = self._soup.find(attrs={'id': 'shipping-details'}).find(attrs={'class': 'price'})

        if not price_el:
            return None

        return parse.parse_money(price_el.text.strip())

    @property
    def title(self):
        try:
            return self._soup.find(attrs={'id': 'title'}).text
        except AttributeError:
            return 'Unknown'

    @property
    def description(self):
        try:
            return self._soup.find(attrs={'id': 'vip-ad-description'}).text
        except AttributeError:
            return 'Unknown'

    @property
    def specifications(self):
        attri = self._soup.find(attrs={'id': 'vip-ad-attributes'})
        
        if not attri:
            return {}

        return [{
            el.find(attrs={'class': 'name'}).text.lower():
            el.find(attrs={'class': 'value'}).text.lower()}
            for el in attri.find_all('tr')
        ]

    @property 
    def bids(self):
        soup = self._soup.find(attrs={'id': 'vip-bidding-block'})

        if not soup:
            return []

        return bidding.Biddings(soup)

    @property
    def seller(self):
        return seller.Seller(self._soup.find(
            attrs={'id': 'vip-seller'}
        ))

    @property
    def listed(self):
        soup = str(self._soup.find(attrs={'id': 'displayed-since'}))
        matches = re.search(r'<span>(.*),', soup)

        try:
            return parse.parse_date(matches.group(1))
        except Exception:
            return datetime.now()

    @property
    def view_count(self):
        return int(self._soup.find(attrs={'id': 'view-count'}).text)