from celery.exceptions import Ignore

from secondhandwatch.models.subscription import QueryModel
from secondhandwatch.celery import app
from secondhandwatch.tasks.manager import AdManager
from secondhandwatch.serializers.advertisement import AdvertisementSerializer
from secondhandwatch.models.advertisement import AdvertisementModel

manager = AdManager()

@app.task(name='secondhandwatch.tasks.tasks.get_advertisements')
def get_advertisements(query_id):
    model = QueryModel.objects.get(pk=int(query_id))
    results = manager.find_all(model.search, limit=5)

    print(results)

    for x in results:
        serializer = AdvertisementSerializer(data=x.to_dict())

        if not serializer.is_valid():
            print(serializer.initial_data)
            raise Exception('Advertisement data was invalid: {}'.format(
                serializer.errors))

        serializer.save()

    return query_id