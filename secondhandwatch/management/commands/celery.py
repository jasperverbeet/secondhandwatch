import shlex
import subprocess
import time

from django.core.management.base import BaseCommand
from django.utils import autoreload

def reload():
    try:
        autoreload.main(restart_celery)
    except Exception as ex:
        print(ex)
        time.sleep(3)
        reload()

def restart_celery():
    cmd = 'pkill -9 celery'
    subprocess.call(shlex.split(cmd), shell=True)
    cmd = 'celery -A secondhandwatch worker -l info'
    subprocess.call(shlex.split(cmd), shell=True)


class Command(BaseCommand):

    def handle(self, *args, **options):
        print('Starting celery worker with autoreload...')
        reload()