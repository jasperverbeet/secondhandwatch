#!/usr/bin/env python
"""
This module is used for advertisement related serializers.
"""

from rest_framework import serializers
from secondhandwatch.models.advertisement import AdvertisementModel, SellerModel, BiddingModel, ImageModel, SpecificationModel


class SpecificationSerializer(serializers.ModelSerializer):
    """ Serialize specifications. """

    class Meta:
        model = SpecificationModel
        fields = ('key', 'value',)


class SellerSerializer(serializers.ModelSerializer):
    """ Serialize seller. """

    class Meta:
        model = SellerModel
        fields = ('__all__')


class BiddingSerializer(serializers.ModelSerializer):
    """ Serialize bidding. """

    class Meta:
        model = BiddingModel
        fields = ('name', 'price', 'date',)


class ImageSerializer(serializers.ModelSerializer):
    """ Serialize image. """

    class Meta:
        model = ImageModel
        fields = ('url',)


class AdvertisementSerializer(serializers.ModelSerializer):
    """ Serialize queries. """
    specifications = SpecificationSerializer(many=True)
    images = ImageSerializer(many=True)
    bids = BiddingSerializer(many=True)
    seller = SellerSerializer(many=False)

    class Meta:
        model = AdvertisementModel
        fields = ('marktplaats_id', 'is_commercial', 'can_deliver',
                  'can_collect', 'shipping_costs', 'title', 'price', 'price_str',
                  'description', 'url', 'specifications', 'images', 'listed',
                  'views', 'bids', 'seller',)

    def create(self, validated_data):
        seller = validated_data.pop('seller')
        specifications = validated_data.pop('specifications')
        images = validated_data.pop('images')
        bids = validated_data.pop('bids')

        advertisement = AdvertisementModel.objects.create(**validated_data)
        # instance.seller = SellerModel.objects.create(**seller)

        for spec_data in specifications:
            SpecificationModel.objects.create(
                advertisement=advertisement, **spec_data
            )

        for img_data in images:
            ImageModel.objects.create(
                advertisement=advertisement, **img_data
            )

        for bid_data in bids:
            BiddingModel.objects.create(
                advertisement=advertisement, **bid_data,
            )

        return advertisement
