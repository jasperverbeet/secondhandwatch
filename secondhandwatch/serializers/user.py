#!/usr/bin/env python
"""
This module is used for user related serializers.
"""

from django.contrib.auth.models import User
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    """ Serialize users. """

    class Meta:
        model = User
        fields = ('username', 'password', 'email', 'first_name', 'last_name')
