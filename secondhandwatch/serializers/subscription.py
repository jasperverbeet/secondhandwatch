#!/usr/bin/env python
"""
This module is used for subscription related serializers.
"""

from rest_framework import serializers
from secondhandwatch.models.subscription import QueryModel, SubscriptionModel

class QuerySerializer(serializers.ModelSerializer):
    """ Serialize queries. """  
    interval = serializers.SerializerMethodField()


    class Meta:
        model = QueryModel
        fields = ('name', 'created', 'interval', 'search', 'is_active')

    def get_interval(self, obj):
        return obj.interval.seconds

    def get_or_create(self):
        defaults = self.validated_data.copy()
        identifier = defaults.pop('search')
        return QueryModel.objects.get_or_create(
            search=identifier, defaults=defaults)


class SubscriptionSerializer(serializers.ModelSerializer):
    """ Serialize subscriptions. """
    query = serializers.SerializerMethodField()

    class Meta:
        model = SubscriptionModel
        fields = ('created', 'query', 'user',)

    def get_query(self, obj):
        """ Get subscriptions query """
        return QuerySerializer(QueryModel.objects.get(pk=obj.query_id)).data