"""
This module is used for subscription related models.
"""

from datetime import timedelta
from django.db import models
from django.contrib.auth.models import User

class QueryModel(models.Model):
    """
    QueryModel is used to process a query. By default a query is ran every hour,
    but more volatile ads can have a shorter interval while less volatile ads
    should have longer intervals.
    """
    name = models.CharField(
        max_length=100,
        blank=True,
        default='',)

    created = models.DateTimeField(
        auto_now_add=True
    )

    interval = models.DurationField(
        default=timedelta(hours=1)
    )

    search = models.CharField(
        max_length=100,
        blank=True,
        default='',)

    is_active = models.BooleanField(
        default=True
    )

    class Meta:
        ordering = ('created',)


class SubscriptionModel(models.Model):
    """
    A user can subscribe to a query model.
    """
    created = models.DateTimeField(
        auto_now_add=True
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='subscription_user',
    )
    query = models.ForeignKey(
        QueryModel,
        on_delete=models.CASCADE,
        related_name='subscription_query',
    )
