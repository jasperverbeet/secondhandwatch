"""
This module is used for advertisement related models.
"""

from django.db import models


class SellerModel(models.Model):
    name = models.CharField(
        max_length=255,
        blank=True,
        default='',
    )

    years_active = models.FloatField(
        default=None,
        null=True
    )

    location = models.CharField(
        max_length=50,
        blank=True,
        default='',
    )

    seller_id = models.CharField(
        max_length=50,
        blank=True,
        default='',
    )

    url = models.URLField(
        max_length=255,
    )


class AdvertisementModel(models.Model):
    """
    Advertisement model is used to keep track of gathered advertisements
    """
    marktplaats_id = models.CharField(
        max_length=50,
        blank=True,
        default='',)

    is_commercial = models.NullBooleanField(
        null=True,
        default=False
    )

    can_deliver = models.NullBooleanField(
        default=None,
        null=True,
    )

    can_collect = models.NullBooleanField(
        default=None,
        null=True,
    )

    shipping_costs = models.FloatField(
        default=None,
        null=True
    )

    title = models.CharField(
        max_length=255,
        blank=True,
        default='',
    )

    price = models.FloatField(
        default=None,
        null=True
    )

    price_str = models.CharField(
        max_length=255,
        blank=True,
        default='',
    )

    description = models.TextField(
        blank=True,
        default='',
    )

    url = models.URLField(
        max_length=255,
    )

    listed = models.DateTimeField()

    views = models.IntegerField()

    # seller = models.ForeignKey(
    #     SellerModel,
    #     related_name="seller",
    #     on_delete=models.CASCADE)


class SpecificationModel(models.Model):
    key = models.CharField(
        max_length=255,
        blank=True,
        default='',
    )
    value = models.CharField(
        max_length=255,
        blank=True,
        default='',
    )

    advertisement = models.ForeignKey(
        AdvertisementModel,
        related_name="spec_seller",
        on_delete=models.CASCADE)


class ImageModel(models.Model):
    url = models.URLField(
        max_length=255,
    )

    advertisement = models.ForeignKey(
        AdvertisementModel,
        related_name="img_seller",
        on_delete=models.CASCADE)


class BiddingModel(models.Model):
    name = models.CharField(
        max_length=255,
        blank=True,
        default='',
    )

    price = models.FloatField(
        default=None,
        null=True
    )

    date = models.DateField()

    advertisement = models.ForeignKey(
        AdvertisementModel,
        related_name="bid_seller",
        on_delete=models.CASCADE)
