from django.contrib import admin
from secondhandwatch.models import subscription, advertisement

admin.site.register(subscription.SubscriptionModel)
admin.site.register(subscription.QueryModel)

admin.site.register(advertisement.AdvertisementModel)
admin.site.register(advertisement.BiddingModel)
admin.site.register(advertisement.ImageModel)
admin.site.register(advertisement.SellerModel)
admin.site.register(advertisement.SpecificationModel)
