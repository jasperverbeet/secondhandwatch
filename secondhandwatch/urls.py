"""
Secondhandwatch URL Configuration
"""

from django.contrib import admin
from django.conf.urls import url
from rest_framework.authtoken import views
from .views import subscribe, account

urlpatterns = [
    url(r'^subscription/(?P<primary_key>[0-9]{1})/$',
        subscribe.SubscriptionViewSet.as_view({'get': 'retrieve'}),
        name='subscription'),
    url(r'subscribe/$',
        subscribe.SubscriptionViewSet.as_view({'post': 'subscribe'}),
        name='subscribe'),
    url(r'^user/', account.UserView.get, name='user'),
    url(r'^auth/', views.obtain_auth_token),
    url(r'^admin/', admin.site.urls),
]
