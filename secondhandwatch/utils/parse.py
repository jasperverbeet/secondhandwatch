import locale
from datetime import datetime

locale.setlocale(locale.LC_ALL, '')

def parse_date(date_str):
    months = ['jan.','feb.', 'mrt.', 'apr.', 'mei', 'jun.', 'jul.', 'aug.', 'sep.', 'okt.', 'nov.', 'dec.']
    dp = date_str.split(' ')

    if len(dp) != 3:
        return None
    
    try:
        day = int(dp[0])
        month = months.index(dp[1]) + 1
        year = int('20{0}'.format(dp[2][1:]))
        return datetime(year, month, day)
    except:
        return 0

def parse_money(money_str):
    mstr = money_str.replace('\u20ac ', '')
    mstr = mstr.replace('.', '')
    mstr = mstr.replace(',', '.')

    try:
        return locale.atof(mstr)
    except:
        return -0.0